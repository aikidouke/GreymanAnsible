# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias lss='ls'
alias lssss='ls'
alias lsss='ls'
alias ls='ls --color=auto'

alias th='task end:today habi | tail -n+4 | head -n-2'

set -o vi
