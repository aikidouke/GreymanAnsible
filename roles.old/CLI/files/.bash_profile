# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

alias lss='ls'
alias lsss='ls'
alias ls='ls --color=auto'
alias cdd='cd ..'
alias cd2='cd ../..'
export BG=brownshrine.jpg
