#!/bin/bash

scp -r /usr/share/terminfo/r oliver@192.168.0.5:/home/oliver/.terminfo/
git config --global user.name aikidouke
git config --global user.email greyman@insight.rr.com
mkdir Source
cd Source
git clone git@gitlab.com:aikidouke/dotfi_main.git dotfiles
cd dotfiles
for i in dir_colors gtk i3 ksh profile spectrwm task tmux vim Xresources; do stow -t ~ $i; done
cd ~/Source
git clone git@gitlab.com:aikidouke/GreymanAnsible.git GLABAnsible
curl -fLo ~/.vim/autoload/plug.vim --create-dirs     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
